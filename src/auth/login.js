import React, {Component} from "react";
import {authenticationService} from "../_services/authentication.service";
import GoogleLogin from 'react-google-login'

class Login extends Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
        this.handleLogin = this.handleLogin.bind(this);
        this.handleEmailInput = this.handleEmailInput.bind(this);
        this.handlePasswordInput = this.handlePasswordInput.bind(this);
    }

    componentDidUpdate() {
        console.log("DID UPDATE IN LOGIN " + authenticationService.isLoggedIn());
        if(authenticationService.isLoggedIn()) {
            this.props.history.push('/');
        }
    }

    handleEmailInput(event){
        this.setState({
            email: event.target.value
        });
    }

    handlePasswordInput(event){
        this.setState({
            password: event.target.value
        });
    }

    handleLogin = event => {
        event.preventDefault();
        const email = this.state.email;
        const password = this.state.password;
        authenticationService.login(email, password).then(() => {
            this.props.history.push('/');
        });

    }

    onGoogleLogin = (response) => {
        authenticationService.googleLogin(response).then(() => {
            this.props.history.push('/');
        }).catch(() => {
            this.props.history.push('/signup');
        });
    }

    render(){
        return(
            <div className="content">
            <div className="container ">
            <div className="row login-body">
                <div className="container top-double-buffer">
                    <span className="mx-auto green underlined-text-green fs-xlarge font-bold">
ورود به لقمه!                        </span>
                </div>
            </div>
            <div className="row">
            <div className="container">
                <div className="content-box top-bottom-buffer">
                    <div className="row justify-content-center top-bottom-buffer fs-little">
                        <GoogleLogin
                            clientId="38170085199-b2r48ppgthlt3b3oe3enojl65ab3bqbp.apps.googleusercontent.com"
                            buttonText="ورود با استفاده از گوگل"
                            onSuccess={this.onGoogleLogin}
                            onFailure={(response) => {console.log(response)}}
                            cookiePolicy={'single_host_origin'}
                        />
                    </div>
                    <div className="row justify-content-center">
                        <form className="pl-3 pr-3" onSubmit={this.handleLogin}>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <input type="email" className="form-control" id="inputEmail4" placeholder="نشانی ایمیل"
                                        required value={this.state.email} onChange={this.handleEmailInput}/>
                                </div>

                                <div className="form-group col-md-6">
                                    <input type="password" className="form-control" id="inputPassword4"
                                        placeholder="رمز عبور" required value={this.state.password} onChange={this.handlePasswordInput}/>
                                </div>
                            </div>


                            <button type="submit" className="btn btn-primary">ورود</button>
                        </form>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <a className="signuphref" href="/signup"> برای ثبت نام کلیک کنید</a>
                </div>
            </div>
            </div>
            </div>
            </div>
        );
    }
}

export default Login;