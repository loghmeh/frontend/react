import React, {Component} from "react";
import {trackPromise} from "react-promise-tracker";
import axios from "axios";
import {authenticationService} from "../_services/authentication.service";
import API_BASE_URL from '../config'
class SignUp extends Component{

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            phoneNumber: ''
        }
        this.handleSignUp = this.handleSignUp.bind(this);
        this.handleEmailInput = this.handleEmailInput.bind(this);
        this.handlePasswordInput = this.handlePasswordInput.bind(this);
        this.handleFirstNameInput = this.handleFirstNameInput.bind(this);
        this.handleLastNameInput = this.handleLastNameInput.bind(this);
        this.handlePhoneNumberInput = this.handlePhoneNumberInput.bind(this);
    }

    handleEmailInput(event){
        this.setState({
            email: event.target.value
        });
    }

    handlePasswordInput(event){
        this.setState({
            password: event.target.value
        });
    }

    handleFirstNameInput(event){
        this.setState({
            firstName: event.target.value
        });
    }

    handleLastNameInput(event){
        this.setState({
            lastName: event.target.value
        });
    }

    handlePhoneNumberInput(event){
        this.setState({
            phoneNumber: event.target.value
        });
    }


    handleSignUp = event => {
        event.preventDefault();
        const firstName = this.state.firstName;
        const lastName = this.state.lastName;
        const email = this.state.email;
        const password = this.state.password;
        const phoneNumber = this.state.phoneNumber;
        trackPromise(
            axios.post(`${API_BASE_URL}/auth/register`,{
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                phoneNumber: phoneNumber
            }).then(res => {
                authenticationService.login(email, password).then(() => {
                    this.props.history.push('/');
                });
            })
            .catch(err => {
                if (err.response) alert(err.response.data.message);
                else alert('[Error] In Sending Request');
            })
        );

    }
    render(){
        return(
            <div className="content">
                <div className="container">
                    <div className="row login-body">
                        <div className="container top-double-buffer">
                    <span className="mx-auto green underlined-text-green fs-xlarge font-bold">
ثبت نام در لقمه!                        </span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="container">

                            <div className="content-box top-bottom-double-buffer">

                                <div className="container">
                                    <div className="row justify-content-center">
                                        <form className="pl-3 pr-3" onSubmit={this.handleSignUp}>
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <input type="email" className="form-control" id="inputEmail4"
                                                           placeholder="نشانی ایمیل" required value={this.state.email} onChange={this.handleEmailInput}/>
                                                </div>

                                                <div className="form-group col-md-6">
                                                    <input type="password" className="form-control" id="inputPassword4"
                                                           placeholder="رمز عبور" required value={this.state.password} onChange={this.handlePasswordInput}/>
                                                </div>
                                            </div>

                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <input type="text" className="form-control" id="firstName"
                                                           placeholder="نام" value={this.state.firstName} onChange={this.handleFirstNameInput}/>
                                                </div>

                                                <div className="form-group col-md-6">
                                                    <input type="text" className="form-control" id="lastName"
                                                           placeholder="نام خانوادگی" value={this.state.lastName} onChange={this.handleLastNameInput}/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="inputAddress2"
                                                       placeholder="شماره موبایل" value = {this.state.phoneNumber} onChange={this.handlePhoneNumberInput}/>
                                            </div>

                                            <div className="form-group">
                                                <div className="form-check form-check-inline mr-0">
                                                    <input className="form-check-input" type="checkbox" id="gridCheck"
                                                           required/>
                                                        <label className="form-check-label form-text pr-1"
                                                               htmlFor="gridCheck">
                                                            ضوابط ایجاد حساب کاربری در لقمه! را خوانده‌ام و با آن موافقت
                                                            دارم.
                                                        </label>
                                                        <div className="invalid-feedback">
                                                            در صورت درخواست ایجاد حساب کاربری، باید شرایط عضویت در لقمه!
                                                            را بپذیرید.
                                                        </div>
                                                </div>
                                            </div>
                                            <button type="submit" className="btn btn-primary">ایجاد حساب در لقمه!
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SignUp;