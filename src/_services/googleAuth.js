
import Axios from 'axios';
import API_BASE_URL from '../config'

const onGoogleLoginSuccess = (response) => {
  const idToken = response.tokenId;
  let req = Axios.post(`${API_BASE_URL}/auth/google`, {
    "idToken": idToken
  });
  return req;
}

const onGoogleLoginFailure = (response) => {
  console.log(response);
}

export { onGoogleLoginFailure, onGoogleLoginSuccess }
