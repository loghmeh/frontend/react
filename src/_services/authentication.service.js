import axios from 'axios';
import {onGoogleLoginSuccess} from './googleAuth'
import jwt from 'jsonwebtoken'
import API_BASE_URL from '../config'

function JWT() { 
    return localStorage.getItem('JWT');
}
function isLoggedIn() {
    if(!localStorage.getItem("isLoggedIn") === 'true') return false;
    const token = JWT();
    if (!token) return false;
    var decodedToken = jwt.decode(token, {complete: true});
    var dateNow = new Date();
    console.log(decodedToken.payload);
    const timeNowSec = parseInt(dateNow.getTime() / 1000);
    console.log(decodedToken.payload.exp + " " + timeNowSec);
    return decodedToken.payload.exp >= timeNowSec;
} 

export var authenticationService = {
    login,
    googleLogin,
    logout,
    JWT,
    isLoggedIn,
}

function login(email, password) {
    let req = axios.post(`${API_BASE_URL}/auth/login`,{
        email: email,
        password: password
    });
        
    req.then(res => {
        localStorage.setItem('JWT', res.data.JWT);
        localStorage.setItem('isLoggedIn', 'true');
    }).catch(err =>{
        if (err.response) alert(err.response.data.message);
        else alert('[Error] In Sending Request');
    }
    );
    return req;
}

function googleLogin(response) {
    const req = onGoogleLoginSuccess(response);
    req.then((resp) => {
        localStorage.setItem('JWT', resp.data.JWT);
        localStorage.setItem('isLoggedIn', 'true');
    });
    return req;
}

function logout() {
    localStorage.removeItem('JWT');
    localStorage.setItem('isLoggedIn', 'false');
}