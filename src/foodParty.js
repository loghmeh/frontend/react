import React from "react";
import Modal from "./modal";
import FoodPartyDetail from "./foodPartyDetail";
import starLogo from "./assets/icons/star.svg";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
class FoodPartyItem extends React.Component {
    constructor(props) {
        super(props);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.state = {
            show: false
        }
    }
    showModal = () => {
        this.setState({ show: true });
    };
    hideModal = () => {
        this.setState({ show: false });
    };
    handleAdd = () => {
        if (this.props.count === 0) {
            alert('this food has been finished!');
        }
        else {
            const food = this.props.food;
            const restaurantId = this.props.restaurantId;
            this.props.onAdd(food.id, restaurantId);
        }
    }
    handleIncrease = () => {
        const restaurantId = this.props.restaurantId;
        this.props.handleIncrease(this.props.food.id, restaurantId);
    }
    handleDecrease = () => {
        const restaurantId = this.props.restaurantId;
        this.props.handleDecrease(this.props.food.id, restaurantId);
    }
    render() {
        const food = this.props.food;
        const restaurantId = this.props.restaurantId;
        const restaurantName = this.props.restaurantName;
        return (
            <div className="food-menu-item bottom-double-buffer">
                <Modal show={this.state.show} onClose={this.hideModal} >
                    <FoodPartyDetail
                        food={food}
                        restaurantId={restaurantId}
                        restaurantName={restaurantName}
                        // hideModal={this.hideModal}
                        handleIncrease={this.handleIncrease}
                        handleAdd={this.handleAdd}
                        handleDecrease={this.handleDecrease}
                    />
                </Modal>
                <div className="mb-2 mt-2 mr-4 ml-4">
                    <div className="container h-100 bg-white-bordered">
                        <div className="row top-buffer justify-content-center">
                            <img src={food.image} className="rounded img-fluid food-thumbnail" alt="food-thumbnail" />
                        </div>
                        <div className="row justify-content-center">
                            <div className="container">
                                <span
                                    className="my-auto fs-tiny ml-2 font-bold"
                                    onClick={this.showModal}
                                >
                                    {food.name}
                                </span>

                                <span className="d-inline-block my-auto mr-2">
                                    <span className="fs-tiny font-thin">{food.popularity}</span>
                                    <span>
                                        <img className="icon-star" src={starLogo} alt="star" />
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div className="row">
                            <div className="container">
                                <div className="col-md-1"></div>
                                <div className="col">
                                    <span className="fs-tiny font-thin">{food.price} تومان</span>
                                </div>
                                <div className="col">
                                    <span className="fs-tiny font-thin old-price"><del className="fs-tiny font-thin">{food.oldPrice}</del> تومان</span>
                                </div>
                                <div className="col-md-1"></div>
                            </div>
                        </div>

                        <div className="row bottom-buffer">
                            <div className="container">
                                <div className="col-md" />
                                <div className="col">
                                    <button type="button" onClick={this.handleAdd} className="btn btn-add-to-cart">افزودن به سبد خرید</button>
                                </div>
                                <div className="col">
                                    <span type="text" className="foodparty-counter">{food.counter}</span>
                                </div>
                                <div className="col-md" />
                            </div>
                        </div>
                        <div className="bottom-buffer justify-content-center foodPartyItem border-top">
                            <div className="container">
                                <span type="text" className="fs-little">{restaurantName}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


class FoodPartySlider extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }
    addToCart = (foodId, restaurantId) => {
        this.props.addToCart(foodId, restaurantId);
    }
    handleIncrease = (foodId, restaurantId) => {
        this.props.increase(foodId, restaurantId);
    }
    handleDecrease = (foodId, restaurantId) => {
        this.props.decrease(foodId, restaurantId);
    }
    render() {
        const rests = this.props.restaurants;
        const listRests = Object.values(rests).map((rest) => {
            const foods = rest.menu;
            const restaurantId = rest.id;
            const restaurantName = rest.name;
            const listFoods = Object.values(foods).map((food) => {
                let cartCount = 0;
                const foodIfInCart = this.props.cart.foods.find((item) =>
                    item.id === food.id
                )
                if (foodIfInCart) {
                    cartCount = foodIfInCart.cartCount;
                }
                food.cartCount = cartCount;
                return (
                    <FoodPartyItem
                        key={food.id}
                        food={food}
                        restaurantId={restaurantId}
                        restaurantName={restaurantName}
                        onAdd={(foodId, restaurantId) => this.addToCart(foodId, restaurantId)}
                        handleIncrease={this.handleIncrease}
                        handleDecrease={this.handleDecrease}
                    />
                );
            });
            return (listFoods);
        });

        let finalListRests = []
        listRests.forEach((element) => {
            element.forEach(innerElem => finalListRests.push(innerElem) )
        })

        var sliderSettings = {
            dots: true,
            infinite: true,
            // autoplay: true,
            speed: 2500,
            slidesToShow: finalListRests.length,
            // slidesToScroll: 1
        };

        return (
            <div>
                <div className="">
                    <div className="slider">
                        <Slider {...sliderSettings}>
                            {finalListRests}
                        </Slider>
                    </div>
                </div>
            </div>
        );
    }
}

export default FoodPartySlider
