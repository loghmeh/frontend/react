import React from "react";

class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.increase = this.increase.bind(this);
        this.decrease = this.decrease.bind(this);
    }
    increase() {
        this.props.onIncrease();
    }
    decrease() {
        this.props.onDecrease();
    }
    render() {
        const value = this.props.value;
        return (

            <div className="d-flex">
                <span className="flaticon-minus" onClick={this.decrease}></span>
                <span className="pl-2 pr-2">{value}</span>
                <span className="flaticon-plus" onClick={this.increase}></span>
            </div>
        );
    }
}

export default Counter
