import React from 'react'
// import Navbar from './navbar';
import './assets/css/header.css'
import loghmehLogo from './assets/icons/logo.png'
class Header extends React.Component {
  render() {
    if (this.props.isProfilePage) {
      const user = this.props.user;
      const userFullName = user && user.firstName + ' ' + user.lastName;
      const userPhoneNumber = user && user.phoneNumber;
      const userEmail = user && user.email;
      const userBalance = user && user.balance;
      return (
        <div className="header">
          <div className="container header-content h-100 w-100">
            <div className="row h-100 w-100 top-bottom-double-buffer">
              <div className="col-8 h-100 my-auto text-right d-flex align-items-center">
                <div>
                  <i className="flaticon-account profile-picture-icon profile-picture-icon"></i>
                  <span className="pr-2 white font-bold small-account-text-for-mobile">{userFullName}</span>
                </div>
              </div>

              <div className="col-4 my-auto white">
                <div className="row">
                  <span className="d-inline-block d-flex">
                    <i className="flaticon-phone profile-header-icon header-icon-mobile" ></i>
                    <span className="mr-3 my-auto font-thin fs-little small-text-for-mobile">{userPhoneNumber}</span>
                  </span>
                </div>

                <div className="row">
                  <span className="d-inline-block d-flex">
                    <i className="flaticon-mail profile-header-icon header-icon-mobile"></i>
                    <span className="mr-3 my-auto font-thin fs-little small-text-for-mobile">{userEmail}</span>
                  </span>
                </div>

                <div className="row">
                  <span className="d-inline-block d-flex">
                    <i className="flaticon-card profile-header-icon header-icon-mobile"></i>
                    <span className="mr-3 my-auto font-thin fs-little small-text-for-mobile">{userBalance} تومان</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
      
    }
    else if(this.props.isHomePage){
        return(
            <div className="header-background">
              <div className="red-overlay">
                <div className="header-content">
                  <div className="container">
                    <img src={loghmehLogo} className="header-logo" alt="header"/>
                      <div>
                        <p className="white fs-medium text-shadow">
                          اولین و بزرگ‌ترین وب‌سایت سفارش آنلاین غذا در دانشگاه تهران
                        </p>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        )
    }
    else {
      return (
        <div className="header header-empty">
        </div>
      )
    }
  }
}

export default Header
