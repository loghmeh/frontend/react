import React from 'react'
import './assets/css/navbar.css';
import logo from './assets/icons/logo.png'
import Modal from './modal'
import CartContent from './cart/CartContent'
import {authenticationService} from "./_services/authentication.service";

class Nabvar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showCartModal: false
        }
    }

    increaseCartItem = (foodId) => {
        const restaurantId = this.props.cart.restaurantId;
        this.props.handleIncrease(foodId, restaurantId);
    }

    decreaseCartItem = (foodId) => {
        const restaurantId = this.props.cart.restaurantId;
        this.props.handleDecrease(foodId, restaurantId);
    }

    showCartModal = () => {
        this.setState({
            showCartModal: true
        });
    }

    closeCartModal = () => {
        this.setState({
            showCartModal: false
        });
    }

    onFinalizeCart = () => {
        this.props.onFinalizeCart();
    }

    componentDidUpdate() {
        console.log("DID UPDATE IN LOGOUT");
        console.log("in didUpdate " + authenticationService.isLoggedIn())
    }

    handleLogOut = () => {
        authenticationService.logout();
        console.log("in HandleLogout " + authenticationService.isLoggedIn())
        this.props.history.push('/');
    }

    render() {
        const isLoggedIn = authenticationService.isLoggedIn();
        const isProfilePage = this.props.isProfilePage;
        const showAccount = isLoggedIn && !isProfilePage;
        let cartCounter = 0;
        this.props.cart.foods.forEach(food => {
            cartCounter += food.count;
        })

        // const cartTotal = this.props.cart && this.props.cart.totalPrice;
        // const cartItems = this.props.cart
        //     ? this.props.cart.foods
        //     : [];

        return (
            <nav className="navbar fixed-top navbar-expand-sm navbar-light bg-light float-right">
                <div className="container-fluid loghmeh-navbar">
                    <Modal show={this.state.showCartModal} onClose={this.closeCartModal}>
                        <CartContent
                            cart={this.props.cart}
                            onFinalize={this.onFinalizeCart}
                            increase={(foodId) => this.increaseCartItem(foodId)}
                            decrease={(foodId) => this.decreaseCartItem(foodId)}
                        />

                    </Modal>
                    <a className="navbar-brand" href="/"><img src={logo} alt="لقمه"></img></a>

                    <div className="collapse navbar-collapse" id="nav-content">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item cart-li">
                                <div className="cart">
                                    <div 
                                        className="nav-link"
                                        onClick={this.showCartModal}    
                                    >
                                        <i className="flaticon-smart-cart"></i>
                                        <span className="cart-counter">{cartCounter}</span>
                                    </div>
                                </div>
                            </li>
                            {showAccount &&
                                <li className="nav-item ">
                                    <a className="nav-link" href="/profile">حساب کاربری</a>
                                </li>
                            }
                            {isLoggedIn
                                ? (
                                    <li className="nav-item nav-logout">
                                        <a className="nav-link" onClick={this.handleLogOut}>خروج</a>
                                    </li>
                                )
                                : (
                                    <li className="nav-item nav-logout">
                                        <a className="nav-link" href="/">ورود</a>
                                    </li>
                                )}
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Nabvar
