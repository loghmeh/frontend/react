import React from "react";
import {NavLink} from "react-router-dom";

function RestaurantItem(props){
    const restaurant = props.restaurant;
    const restUrl = "/restaurant/" + restaurant.id;
    return(
            <div className="col-md-3 food-menu-item bottom-double-buffer">
                <div className="container h-100 bg-white-bordered">

                    <div className="row top-buffer justify-content-center">
                        <img src={restaurant.logo} className="rounded img-fluid food-thumbnail"
                            alt="food-thumbnail"/>
                    </div>
                    <div className="row justify-content-center">
                        <div className="container">
                            <span className="my-auto fs-tiny ml-2 font-bold">{restaurant.name}</span>
                        </div>
                    </div>


                    <div className="row bottom-buffer justify-content-center">
                        <div className="container">
                            <NavLink
                                exact to={restUrl}
                                type="button"
                                className="btn btn btn-show-menu"
                            >
                                نمایش منو
                            </NavLink>
                        </div>
                    </div>
                </div>
                </div>
    );
}
function RestaurantList(props){
    const restaurants = props.restaurants;
    const listRests = restaurants.map((restaurant) => <RestaurantItem restaurant={restaurant}/>)
    // var i;
    // var j;
    // const restStyledList = [];
    // for (i = 0; i < restaurants.length - 4; i++) {
    //     const restRow = [];
    //     for(j = i; j < (restaurants.length) && j < i + 4 ;j++){
    //         restRow.push(restaurants[j]);
    //     }
    //     restStyledList.push(restRow);
    // }
    // const listRests = Object.values(restStyledList).map((restRow)=>{
    //     const restItem = Object.values(restRow).map((restaurant)=>
    //     {
    //         return (
    //             <RestaurantItem restaurant={restaurant}/>
    //         );
    //     });
    //     return(
    //         <div className="row">
    //             {restItem}
    //         </div>);
    //     });

    return(
        <div className="row restaurant-list">
            {/* <div className="col-md-0.5"/> */}
            {/* <div className="col"> */}
            {listRests}
            {/* </div> */}
            {/* <div className="col-md-0.5"/> */}
        </div>
    )
}

export default RestaurantList