import React from "react";
import Counter from '/counter';
import axios from 'axios';

function FoodItem(props) {
    const name = props.name;
    const price = props.quantity;
    const thumbUrl = props.thumbUrl;
    const rating = props.rating;
    const isFinished = props.isFinished;

    return (
        <div className="col-md-4 food-menu-item">
            <div className="container h-100 bg-white-bordered">
                <div className="row top-buffer justify-content-center">
                    <img src={thumbUrl} className="rounded img-fluid food-thumbnail" alt="food-thumbnail" />
                </div>
                <div className="row justify-content-center">
                    <div className="container">
                        <span className="my-auto fs-tiny ml-2 font-bold">{name}</span>
                        <span className="d-inline-block my-auto mr-2">
                            <span className="fs-tiny font-thin">{rating}</span>
                            <span>
                                <img className="icon-star" src="assets/icons/star.svg" alt="star" />
                            </span>
                        </span>
                    </div>
                </div>

                <div className="row justify-content-center">
                    <div className="container">
                        <span className="fs-tiny font-thin">{price}</span>
                    </div>
                </div>

                <div className="row bottom-buffer justify-content-center">
                    <div className="container">
                        {isFinished
                            ? (<button type="button" className="btn btn-add-to-cart">افزودن به سبد خرید</button>)
                            : (<button type="button" className="btn btn-not-available" disabled>ناموجود</button>)
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FoodItem
