import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
// import './assets/css/reset/reset.css';
import './assets/css/fonts.css'
import './assets/css/base.css'
import './assets/icons/flaticon/flaticon.css'
import './assets/css/home.css'
import './assets/css/foodPartyDetail.css'
import './assets/css/login.css'
import Profile from './profile'
import Footer from './footer'
import Restaurant from './restaurant'
import { trackPromise } from 'react-promise-tracker';
import HomePage from "./home";
import {
  BrowserRouter as Router,
  Route, Switch, Redirect,
} from "react-router-dom";
import axios from "axios";
import Nabvar from "./navbar";

import { authenticationService } from "./_services/authentication.service"
import SignUp from "./auth/singUp";
import Login from "./auth/login";
import API_BASE_URL from './config'

const PrivateRoute = ({ render: Render, ...rest }) => (
  <Route {...rest} render={(props) => (
    authenticationService.isLoggedIn() === true
      ? Render(props)
      : <Redirect to='/login' />
    )}
  />
)
class Loghmeh extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: {
        foods: [],
        totalPrice: 0,
        restaurantId: null
      },
      foodParty: {
        finishTime: "1970/01/01 00:00:00",
        restaurants: []
      },
      inDashboard: false
    };
  }
  componentDidUpdate() {
    console.log("DID UPDATE IN APP");
    // if(authenticationService.isLoggedIn()) {
    //     this.props.history.push('/');
    // }
  }
  componentDidMount() {
    document.title = "لقمه!";
    const token = authenticationService.JWT();
    trackPromise(
      axios.get(`${API_BASE_URL}/api/cart`,{headers: {"Authorization" : `JWT ${token}`}}).then(
        res => {
          this.setState({
            cart: res.data
          })
        })
    );
    const finishTime = new Date(this.state.foodParty.finishTime).getTime();
    const now = new Date().getTime();
    this.timeout = setTimeout(this.fetchFoodParty, finishTime - now < 0 ? 0 : finishTime - now);

  }
  fetchFoodParty = () => {
    console.log("GETTING FP");
    const token = authenticationService.JWT();
    trackPromise(
      axios.get(`${API_BASE_URL}/api/foodparty`,{headers: {"Authorization" : `JWT ${token}`}}).then(
        res => {
          this.setState({
            foodParty: res.data
          })
          const finishTime = new Date(this.state.foodParty.finishTime).getTime();
          const now = new Date().getTime();
          this.timeout = setTimeout(this.fetchFoodParty, finishTime - now < 0 ? 1000 * 60 : finishTime - now);
        })
        .catch(err => {
          this.setState({
            foodParty: {
              finishTime: "1970/01/01 00:00:00",
              restaurants: []
            }
          })
        })
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  increaseCartItem = (foodId, restaurantId) => {
    const request = {
      foodId: foodId,
      restaurantId: restaurantId
    };
    const token = authenticationService.JWT();
    trackPromise(
      axios.post(`${API_BASE_URL}/api/cart`, {
        foodId: request.foodId,
        restaurantId: request.restaurantId
      },{headers: {"Authorization" : `JWT ${token}`}}).then(res => {
        alert("غذا با موفقیت به سبد خرید اضافه شد.");
        this.setState({
          cart: res.data
        });
      }).catch(err => {
        if (err.response) alert(err.response.data.message);
        else alert('[Error] In Sending Request');
      }));
  }
  addToCartItem = (foodId, restaurantId) => {
    const request = {
      foodId: foodId,
      restaurantId: restaurantId
    };
    const token = authenticationService.JWT();
    trackPromise(
      axios.post(`${API_BASE_URL}/api/cart`, {
        foodId: request.foodId,
        restaurantId: request.restaurantId
      },{headers: {"Authorization" : `JWT ${token}`}}).then(res => {
        this.setState({
          cart: res.data
        });
      }).catch(err => {
        if (err.response) alert(err.response.data.message);
        else alert('[Error] In Sending Request');
      }));
  }

  decreaseCartItem = (foodId, restaurantId) => {
    const request = {};
    request.foodId = foodId;
    request.restaurantId = restaurantId;
    const token = authenticationService.JWT();
    trackPromise(
      axios.delete(`${API_BASE_URL}/api/cart`, {
        headers: {"Authorization" : `JWT ${token}`},
        data: {
          foodId: request.foodId,
          restaurantId: request.restaurantId
        }
      })
        .then(res => {
          this.setState({
            cart: res.data
          });
        }).catch(err => {
          if (err.response) alert(err.response.data.message);
          else alert('[Error] In Sending Request');
        }));

  }

  enteredDashboard = () => {
    this.setState({
      inDashboard: true
    })
  }
  // increaseCartItem(food) {
  exitedDashboard = () => {
    this.setState({
      inDashboard: false
    })
  }

  onFinalizeCart = () => {
    const token = authenticationService.JWT();
    trackPromise(axios.post(`${API_BASE_URL}/api/order`,{},{headers: {"Authorization" : `JWT ${token}`}})
      .then((res) => {
        alert("سفارش شما با موفقیت ثبت شد."); //TODO: Make this a message from backend
        this.setState({
          cart: {
            foods: [],
            totalPrice: 0,
            restaurantId: null
          }
        })
      })
      .catch((err) => {
        if (err.response) alert(err.response.data.message);
        else alert('[Error] In Sending Request');
      }));
  }

  render() {
    const isLoggedIn = authenticationService.isLoggedIn();
    console.log("is logged in? " + isLoggedIn);
    return (
      <div>
        <Router>
          <Route
            render={(props) => (
              <Nabvar
                isProfilePage={this.state.inDashboard}
                onFinalizeCart={this.onFinalizeCart}
                handleIncrease={this.increaseCartItem}
                handleDecrease={this.decreaseCartItem}
                isLoggedIn={isLoggedIn}
                cart={this.state.cart}
                {...props}
              />
            )}
          />

          <Switch>
            <PrivateRoute
              exact path="/restaurant/:id"
              render={(props) => (
                <Restaurant
                  cart={this.state.cart}
                  onFinalizeCart={this.onFinalizeCart}
                  handleIncrease={this.increaseCartItem}
                  handleAdd={this.addToCartItem}
                  handleDecrease={this.decreaseCartItem}
                  isNotInDashboard={this.exitedDashboard}
                  {...props}
                />
              )}
            />
            <PrivateRoute
              exact path="/profile"
              render={(props) => (<Profile isInDashboard={this.enteredDashboard}{...props} />)}
            />
            <PrivateRoute
              exact path="/"
              render={(props) => (
                <HomePage
                  cart={this.state.cart}
                  handleAdd={this.addToCartItem}
                  handleIncrease={this.increaseCartItem}
                  handleDecrease={this.decreaseCartItem}
                  isNotInDashboard={this.exitedDashboard}
                  foodParty={this.state.foodParty}
                  {...props}
                />
              )}
            />
            <Route
              exact path="/signup"
              component={SignUp}
            />
            <Route
              exact path="/login"
              render={(props) => (
                <Login
                  {...props}
                />
              )}
            />
          </Switch>
        </Router>
        <Footer />
      </div>
    );
  }
}


export default Loghmeh;
