import React from "react";
import { trackPromise } from "react-promise-tracker";
import axios from "axios";
import Header from './header';
import FoodPartySlider from "./foodParty";
import RestaurantList from "./restaurantList";
import Timer from "./timer";
import './assets/css/home.css';
import './assets/css/forms.css';
import InfiniteScroll from "react-infinite-scroll-component";
import {authenticationService} from "./_services/authentication.service";
import API_BASE_URL from './config'
class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.pageChunk = 8;
        this.state = {
            restaurants: [],
            isSearching: false,
            pageId: 1,
            hasMore: true,
            searchResults: [],
            searchQuery: {
                searchFood: '',
                searchRestaurant: ''
            }
        }
        this.handleSearch = this.handleSearch.bind(this);
        this.handleFoodSearchQuery = this.handleFoodSearchQuery.bind(this);
        this.handleRestaurantSearchQuery = this.handleRestaurantSearchQuery.bind(this);
    }
    componentDidMount() {
        const pageId = this.state.pageId;
        const pageChunk =this.pageChunk;
        const token = authenticationService.JWT();
        trackPromise(
            axios.get(`${API_BASE_URL}/api/restaurants`,{
                headers: {"Authorization" : `JWT ${token}`},
                params: {
                    pageId: pageId,
                    pageChunk: pageChunk
                }
            }).then(
                res => {
                    this.setState({
                        restaurants: res.data.restaurants,
                        pageId: this.state.pageId + 1
                    })
                })
        );
        this.exitedDashboard();
    }
    increaseCartItem = (foodId, restaurantId) => {
        this.props.handleIncrease(foodId, restaurantId)
    }
    addToCartItem = (foodId, restaurantId) => {
        this.props.handleAdd(foodId, restaurantId);
    }
    decreaseCartItem = (foodId, restaurantId) => {
        this.props.handleDecrease(foodId, restaurantId);
    }
    exitedDashboard = () => {
        this.props.isNotInDashboard();
    }

    handleSearch = (event) => {
        event.preventDefault();
        this.setState({
            isSearching: true,
            hasMore: true,
            searchQuery: {
                searchFood: '',
                searchRestaurant: ''
            }
        });
        const searchFood = this.state.searchQuery.searchFood;
        const foodSearch = searchFood ? searchFood : "";
        const searchRest =  this.state.searchQuery.searchRestaurant;
        const restaurantSearch = searchRest ? searchRest : "";
        const token = authenticationService.JWT();
        trackPromise(
        axios.post(`${API_BASE_URL}/api/search`, {
            foodSearch: foodSearch,
            restaurantSearch: restaurantSearch,
        },{headers: {"Authorization" : `JWT ${token}`}}).then(res => this.setState({
            searchResults: res.data.restaurants,
            restaurants: res.data.restaurants.slice(0, this.pageChunk),
            isSearching: true,
            pageId: 2
        })));

    }

    handleFoodSearchQuery(event){
        this.setState({
            searchQuery: {
                searchFood: event.target.value
            }
        });
    }

    handleRestaurantSearchQuery(event) {
        this.setState({
            searchQuery: {
                searchRestaurant: event.target.value
            }
        });
    }

    fetchMoreData = () => {
        const isSearching = this.state.isSearching;
        const restaurants = this.state.restaurants;
        if(isSearching){
            const searchResults = this.state.searchResults;
            if(restaurants.length >= searchResults.length){
                this.setState({
                    hasMore: false})
                return;
            }
            const pageId = this.state.pageId;
            const pageChunk = this.pageChunk;
            const begin = (pageId - 1) * (pageChunk);
            const end = ((begin + pageChunk ) < searchResults.length) ?  (begin + pageChunk ) : searchResults.length;
            const moreResults = searchResults.slice(begin,end);
            this.setState({
                restaurants: restaurants.concat(moreResults),
                pageId: pageId + 1
            });
        }
        else{
            const pageId = this.state.pageId;
            const pageChunk =this.pageChunk;
            const token = authenticationService.JWT();
            trackPromise(
                axios.get(`${API_BASE_URL}/api/restaurants`,{
                    headers: {"Authorization" : `JWT ${token}`},
                    params: {
                        pageId: pageId,
                        pageChunk: pageChunk
                    }
                }).then(
                    res => {

                        this.setState({
                            restaurants: this.state.restaurants.concat(res.data.restaurants),
                            pageId: this.state.pageId + 1
                        })
                    }).catch(err => {
                        this.setState({hasMore: false})
                    }
                )
            );
        }
    }

    render() {
        return (
            <>
                <Header isHomePage={true} />
                <div className="content">
                    <div className="container">
                        <div className="row justify-content-center">
                            <form className="form-box form-main-search w-50" onSubmit={this.handleSearch}>
                                <div className="form-row search-box">
                                    <div className="col">
                                        <input type="text" className="form-control form-control-lg fs-little p-2"
                                            placeholder="نــــام غــــذا"
                                               value={this.state.searchQuery.searchFood}
                                                onChange={this.handleFoodSearchQuery}/>
                                    </div>
                                    <div className="col">
                                        <input type="text" className="form-control form-control-lg fs-little p-2"
                                            placeholder="نــــام رســــتــــوران"
                                               value={this.state.searchQuery.searchRestaurant}
                                                onChange={this.handleRestaurantSearchQuery}
                                        />
                                    </div>
                                    <div className="col-3">
                                        <button type="submit"
                                            className="btn btn-yellow btn-block fs-little h-100">جســــت‌وجــــو
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="row justify-content-center">
                            <span className="food-party title">
                                جشن غذا!
                            </span>
                        </div>
                        <div className="row justify-content-center foodparty-timer-container">
                            <Timer
                                countTo={this.props.foodParty.finishTime}
                            />
                        </div>
                        <FoodPartySlider
                            restaurants={this.props.foodParty.restaurants}
                            cart={this.props.cart}
                            addToCart={this.addToCartItem}
                            increase={this.increaseCartItem}
                            decrease={this.decreaseCartItem} />
                        <div className="row justify-content-center">
                            <span className="food-party title title-rest-list">
                                رستوران‌ها
                            </span>
                        </div>
                        <InfiniteScroll next={this.fetchMoreData} hasMore={this.state.hasMore} loader={<h4>  در حال جست و جو ...</h4>} dataLength={this.state.restaurants.length} endMessage={
                            <p style={{ textAlign: "center" }}>
                                <b>در انتخاب رستوران وسواس به خرج ندهید!</b>
                            </p>
                        }>
                        <RestaurantList restaurants={this.state.restaurants} />
                        </InfiniteScroll>
                    </div>
                </div>

            </>

        )
    }
}
export default HomePage