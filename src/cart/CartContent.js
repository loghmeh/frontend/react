import React from 'react';
import CartItem from './cartItem';


class CartContent extends React.Component {

    increaseCartItem = (foodId) => {
        this.props.increase(foodId);

    }

    decreaseCartItem(foodId) {
        this.props.decrease(foodId);
    }

    finalizeCart = () => {
        this.props.onFinalize();
    }

    render() {
        const cart = this.props.cart;
        const total = cart.totalPrice;
        const listItems = cart.foods.map((item) =>
            <CartItem
                key={item.id}
                item={item}
                increase={() => this.increaseCartItem(item.id)}
                decrease={() => this.decreaseCartItem(item.id)}
            />
        );

        return (
            <>
                <span className="fs-tiny mx-auto bold underlined-text-grey fs-medium">سبد خرید</span>

                <div className="container-fluid px-0 top-bottom-buffer">
                    <ul className="cart-in-menu-items">
                        <div className="fs-tiny container top-bottom-buffer">
                            {listItems}
                        </div>
                    </ul>
                </div>
                <div className="fs-tiny top-bottom-buffer">
                    جمع کل: <span className="fs-tiny bold">{total}</span>
                </div>

                <div>
                    <button type="button" className="fs-tiny btn btn-finalize-cart" onClick={this.finalizeCart}>تأیید نهایی</button>
                </div>
            </>
        );
    }
}

export default CartContent;