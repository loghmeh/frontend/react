import React from "react";
import CartContent from './CartContent'

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.increaseCartItem = this.increaseCartItem.bind(this);
        this.decreaseCartItem = this.decreaseCartItem.bind(this);
    }

    increaseCartItem(foodId) {
        this.props.increase(foodId);
    }
    decreaseCartItem(foodId) {
        this.props.decrease(foodId);
    }
    onFinalize = () => {
        this.props.onFinalize();
    }
    closeModal = () => {
        this.props.closeModal();
    }
    render() {
        return (
            <div className="container top-bottom-buffer bg-white-bordered">
                <CartContent
                    cart={this.props.cart}
                    onFinalize={this.onFinalize}
                    increase={(foodId) => this.increaseCartItem(foodId)}
                    decrease={(foodId) => this.decreaseCartItem(foodId)}
                />
            </div>

        );
    }

}

export default Cart
