import React from 'react';
import Counter from '../counter';


class CartItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleIncrease = this.handleIncrease.bind(this);
        this.handleDecrease = this.handleDecrease.bind(this);
    }
    handleIncrease() {
        this.props.increase();
    }
    handleDecrease() {
        this.props.decrease();
    }
    cartPrice() {
        const price = parseInt(this.props.item.price);
        const count = parseInt(this.props.item.count);
        const cartPrice = price * count;
        return (cartPrice);
    }
    render() {
        const foodName = this.props.item.name;
        const count = this.props.item.count;
        return (
            <li className="cart-in-menu-item">
                <div className="row no-gutters">
                    <div className="col-md-12">
                        <div className="row no-gutters">
                            <div className="col-sm my-auto">
                                <div className="fs-little">{foodName}</div>
                            </div>
                            <div className="col-sm my-auto">
                                <div className="d-inline-flex cart-interaction">
                                    <Counter
                                        value={count}
                                        onIncrease={this.handleIncrease}
                                        item onDecrease={this.handleDecrease}
                                    />
                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-6"></div>
                            <div className="fs-tiny col-md-6">{this.cartPrice().toString()} تومان</div>
                        </div>
                    </div>
                </div>
            </li>

        );
    }
}

export default CartItem;
