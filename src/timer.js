import React from "react";
import './assets/css/home.css';

export default class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hours: 0,
      minutes: 0,
      seconds: 0
    }
  }
  
  componentDidMount() {
      this.myInterval = setInterval(() => {
        const now = new Date().getTime();
        const countDownDate = new Date(this.props.countTo).getTime();
        const distance = countDownDate - now;
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);
        console.log(this.props.countTo, countDownDate, now);
        if (distance < 0) {
          this.setState({
            hours: 0,
            minutes: 0,
            seconds: 0,
          });
          clearInterval(this.myInterval);
        }
        else {
          this.setState({
            hours: hours,
            minutes: minutes,
            seconds: seconds
          });
        }
      }, 1000)
  }

  componentWillUnmount() {
      clearInterval(this.myInterval)
  }

  render() {
      const { hours, minutes, seconds } = this.state
      return (
        <span type="text" className="foodparty-timer">
          { hours === 0 && minutes === 0 && seconds === 0
                  ? "فودپارتی تمام شد"
                  : <>زمان باقی‌مانده: {hours < 10 ? `0${hours}` : hours}:{minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</>
          }
        </span>
      )
  }
}