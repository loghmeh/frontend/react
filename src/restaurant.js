import React from "react";
import Cart from './cart/cart';
import Header from './header';
import Menu from './menu';
import axios from 'axios';
import { trackPromise } from 'react-promise-tracker';
import './assets/css/restaurant-menu.css'
import {authenticationService} from "./_services/authentication.service";
import API_BASE_URL from './config'

class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.increaseCartItem = this.increaseCartItem.bind(this);
        this.decreaseCartItem = this.decreaseCartItem.bind(this);
        this.addToCartItem = this.addToCartItem.bind(this);
        this.state = {
            id: "",
            logo: "",
            name: "",
            menu: {}
        }
    }
    increaseCartItem(foodId) {
        const restaurantId = this.props.cart.restaurantId;
        this.props.handleIncrease(foodId, restaurantId);
    }
    addToCartItem(foodId) {
        const restaurantId = this.state.id;
        this.props.handleAdd(foodId,restaurantId);
    }
    decreaseCartItem(foodId) {
        const restaurantId = this.props.cart.restaurantId;
        this.props.handleDecrease(foodId,restaurantId);
    }
    componentDidMount() {
        const restaurantId = this.props.match.params.id;
        const token = authenticationService.JWT();
        trackPromise(
            axios.get(`${API_BASE_URL}/api/restaurants?id=${restaurantId}`,{headers: {"Authorization" : `JWT ${token}`}}).then(
                res => {
                    const restaurant = res.data.restaurant;
                    const menu = restaurant.menu;
                    const id = restaurant.id;
                    const name = restaurant.name;
                    const logo = restaurant.logo;
                    this.setState({
                        menu: menu,
                        id: id,
                        name: name,
                        logo: logo,
                    })
                }
            )
        );
        this.exitedDashboard();

    }
    exitedDashboard = () => {
        this.props.isNotInDashboard();
    }
    onFinalizeCart = () => {
        this.props.onFinalizeCart();
    }
    render() {
        const restaurantId = this.state.id;
        return (
            <>
                <Header 
                    isProfilePage={false}
                />

                <div className="content">
                    <div className="container">
                        <div className="row justify-content-center">
                            <img className="restaurant-logo img-thumbnail" src={this.state.logo}
                                alt="restaurant-logo" />

                            <div className="col-12 text-center">
                                <div className="restaurant-name bottom-double-buffer">
                                    {this.state.name}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4"></div>
                            <div className="col-md-8">
                                <span className="mx-auto green underlined-text-green fs-xlarge font-bold">
                                    منوی غذا                        </span>
                            </div>
                        </div>
                        <div
                            className="row flex-column-reverse flex-md-row top-double-buffer">
                            <div className="col-md-4 cart-in-menu">
                                <div className="container">

                                    <div className="row">
                                        <div className="col-md-12">
                                            <Cart
                                                cart={this.props.cart}
                                                onFinalize={this.onFinalizeCart}
                                                increase={(foodId) => this.increaseCartItem(foodId)}
                                                decrease={(foodId) => this.decreaseCartItem(foodId)}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Menu
                                foods={this.state.menu}
                                restaurantId={restaurantId}
                                cartItems={this.props.cart.foods}
                                addToCart={(foodId) => this.addToCartItem(foodId)}
                                increase={this.increaseCartItem}
                                decrease={this.decreaseCartItem}
                            />
                        </div>

                    </div>
                </div>
            </>
        );
    }
}

export default Restaurant
