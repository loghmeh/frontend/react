import React from "react";
import './assets/css/modal.css'

class Modal extends React.Component {

    onClose = () => {
        this.props.onClose();
    }

    render() {
        if (!this.props.show) {
            return null;
        }
        return (
            <div className="modal display-block">
                <section className="modal-main">
                    <div className="content bg-white-bordered">
                        <div className="container top-bottom-buffer">
                            <span
                                className="modal-close"
                                onClick={this.onClose}
                            >
                                &times;
                            </span>
                            {this.props.children}
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Modal
