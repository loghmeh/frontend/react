import React from "react";
import Counter from './counter';
import starIcon from './assets/icons/star.svg'
class FoodPartyDetail extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }
    componentDidMount() {
        // const total = parseInt(this.props.food.price) * parseInt(this.props.food.cartCount)
        this.setState({
            counter: parseInt(this.props.food.cartCount),
        });
    }

    handleIncrease = () => {
        // const price = this.props.food.price;
        const food = this.props.food;
        if(this.props.food.cartCount >= this.props.food.count)
            alert('insufficient food!');
        else{
            if(this.props.food.cartCount === 0){
                this.props.handleAdd(food);
            }
            else {
                this.props.handleIncrease(food);
            }
        }
    }
    handleDecrease = () => {
        const food = this.props.food;
        if (this.props.food.cartCount === 0) {
            alert('you cannot decrease');
        }
        else {
            this.props.handleDecrease(food);
        }
    }
    addToCart() {
        const food = this.props.food;
        this.props.handleAdd(food);
    }
    render() {
        const food = this.props.food;
        // const counter = this.props.food.count;
        const cartCount  = this.props.food.cartCount;
        const restaurantName = this.props.restaurantName;
        return (
            <>
            <div className="row top-buffer justify-content-center">
                <span className="fs-little font-bold">{restaurantName}</span>
            </div>
            <div className="row border-bottom">
            <div className="col-3">
            <img src={food.image} className="rounded img-fluid food-thumbnail"
        alt="food-thumbnail" />
            </div>
        <div className="col">
            <div className="row">
                <span className="my-auto fs-tiny ml-2 font-bold">{food.name}</span>
                <span className="d-inline-block my-auto mr-2">
                            <span className="fs-tiny font-thin">{food.popularity}</span>
                            <span>
                                <img className="icon-star" src={starIcon} alt="star" />
                            </span>
                        </span>
            </div>
            <div className="row">
                <span className="fs-tiny font-thin description-text">{food.description} </span>
            </div>
            <div className="row">
                <span className="fs-tiny font-thin">{food.price} تومان</span>
                <span className={"fs-tiny font-thin old-price"}>{food.oldPrice} تومان</span>
            </div>
        </div>
            </div>

                <div className="row bottom-buffer justify-content-center detail-finalize">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-5">
                                <span type="text" className="foodParty-counter">{food.count} موجودی: </span>
                            </div>
                            <div className="col">
                                <Counter
                                    value={cartCount}
                                    onIncrease={this.handleIncrease}
                                    item onDecrease={this.handleDecrease}
                                />
                            </div>
                            <div className=" col">
                                <button type="button" className="btn btn-add-to-cart-detail " onClick={this.addToCart}>اضافه کردن به سبد خرید
                                </button>
                            </div>

                        </div>
                    </div>

                </div>
                </>);
    }
}
export default FoodPartyDetail
