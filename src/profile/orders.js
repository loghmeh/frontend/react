import React, { Component } from "react";
import Modal from '../modal'
import '../assets/css/factor.css'
class OrdersTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFactor: false
    };
  }

  showFactor = () => {
    this.setState({
      showFactor: true
    });
  }

  render() {
    const orders = this.props.orders.map((order, index) => {
      return (
        <tr>
          <OrderItem
            order={order}
          />
        </tr>
      )
    });
    return (
      <div className="container">
        <div className="row justify-content-center">
          <table className="gfg top-double-buffer" >
            <col className="column-one" />
            <col className="column-two" />
            <col className="column-three" />
            {orders}
          </table>
        </div>
      </div>
    )
  }
}


class OrderItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showFactor: false
    };
  }
  showFactor = () => {
    this.setState({
      showFactor: true
    });
  }

  closeModal = () => {
    this.setState({
      showFactor: false
    });
  }

  render() {
    let orderClassNames;
    let orderButtonText;
    let orderButtonTextOnHover;
    const order = this.props.order;

    if (order.status === 'DELIVERING') {
      orderClassNames = 'label delivery';
      orderButtonText = 'پیک در مسیر';
      orderButtonTextOnHover = 'زمان تحویل در ' + order.eta;
    }
    else if (order.status === 'TODELIVER') {
      orderClassNames = 'label finding-delivery';
      orderButtonText = 'در جستجوی پیک';
      orderButtonTextOnHover = 'زمان تخمینی تحویل در ' + order.eta;
    }
    else {
      orderClassNames = 'btn-view-factor';
      orderButtonText = 'مشاهده‌ی فاکتور';
      orderButtonTextOnHover = 'در زمان ' + order.eta + ' تحویل داده شد.';
    }
    return (
      <>
        <Modal show={this.state.showFactor} onClose={this.closeModal}>
          <OrderFactor
            order={order}
          />
        </Modal>
        <td className="right-column">
          {order.id}
        </td>

        <td>
          {order.restaurant.name}
        </td>

        <td className="left-column">
          <button
            type="button"
            className={"btn " + orderClassNames}
            onClick={this.showFactor}
            title={orderButtonTextOnHover}
            disabled={order.status !== 'DELIVERED'}
          >
            {orderButtonText}
          </button>
        </td>
      </>
    )
  }
}

class OrderFactor extends React.Component {
  closeModal = () => {
    this.props.closeModal();
  }
  render() {
    const order = this.props.order;
    const foodRows = order.foods.map((food, index) => {
      return (
        <tr>
          <td className="geeks">{index + 1}</td>
          <td>{food.name}</td>
          <td>{food.count}</td>
          <td>{food.price}</td>
        </tr>
      )
    })
    return (
        <>
          <div className="row justify-content-center">
            <span className="factor title fs-medium">
              {order.restaurant.name}
            </span>
          </div>
          <div className="row justify-content-center">
            <hr className="line" />
          </div>

          <div className="row pl-4 pr-4 justify-content-center factor">

            <table className="factor-table">
              <tr>
                <th>ردیف</th>
                <th>نام غذا</th>
                <th>تعداد</th>
                <th>قیمت</th>
              </tr>
              {foodRows}
            </table>
          </div>
        </>
    );
  }
}

export default OrdersTab;
