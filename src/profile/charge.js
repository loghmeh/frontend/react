import React, { Component } from "react";
import axios from 'axios';
import { trackPromise } from 'react-promise-tracker';
import { authenticationService } from "../_services/authentication.service"
import API_BASE_URL from '../config'

class ChargeTab extends Component {
  onSuccessfulCharge = (chargeAmount) => {
      this.props.onSuccessfulCharge(chargeAmount);
  }
  render() {
    return (
      <div className="container top-double-buffer">
        <div className="row justify-content-center top-double-buffer">
          <IncreaseBalanceForm
            onSuccessfulCharge={this.onSuccessfulCharge}
          />
        </div>
      </div>
    )
  }
}

class IncreaseBalanceForm extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      values: {
        amount: ''
      },
      isSubmitting: false,
      isError: false
    }
  }

  handleChange(event) {
    this.setState({
      values: {
        amount: event.target.value
      }
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    this.setState({ isSubmitting: true }); //TODO: Do something with this in all requests
    const chargeAmount = this.state.values.amount;
    const token = authenticationService.JWT();
    trackPromise(axios.post(`${API_BASE_URL}/api/user/balance`, {
        amount: chargeAmount
    },{headers: {"Authorization" : `JWT ${token}`}}).then(res => {
      this.setState({
        values: {
          amount: ''
        },
        isSubmitting: false,
        isError: false
      });
      this.props.onSuccessfulCharge(chargeAmount);
      alert(res.data.message)
    }).catch(err => {
        if (err.response) alert(err.response.data.message);
        else alert('[Error] In Sending Request');
    }));
  }

  render() {
    return (
      <>
        <form className="w-70" onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-8">
              <input
                type="number"
                min="1000"
                max="100000"
                step="1000"
                className="form-control"
                value={this.state.values.amount}
                onChange={this.handleChange}
                id="charge-amount"
                placeholder="میزان افزایش اعتبار"
                />
              </div>
              <div className="form-group col-md-4 increase-button">
                <button type="submit" className="btn btn-primary submit-button h-100 btn-block">افزایش</button>
              </div>
          </div>
        </form>

        <div className={`message ${this.state.isError && "error"}`}>
          {this.state.isSubmitting ? "لطفاً صبر کنید." : this.state.message}
        </div>
      </>
    )
  }
}


export default ChargeTab;
