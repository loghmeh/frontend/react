import {
  Route,
  NavLink,
  HashRouter,
} from "react-router-dom";
import React from 'react';
import OrdersTab from './orders'
import ChargeTab from './charge'
import '../assets/css/profile.css'
import Header from "../header";
import axios from 'axios';
import { trackPromise } from 'react-promise-tracker';
import {authenticationService} from "../_services/authentication.service";
import API_BASE_URL from '../config';
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: undefined
    }
  }
  dashBoardLoaded = () => {
    this.props.isInDashboard();
  }
  componentDidMount() {
    const token = authenticationService.JWT();
    trackPromise(axios.get(`${API_BASE_URL}/api/user/profile`,{headers: {"Authorization" : `JWT ${token}`}})
    .then((res) => {
      const user = res.data;
      this.setState({
        user: user
      })
    }));
    this.dashBoardLoaded();
  }

  onSuccessfulCharge = (chargeAmount) => {
    const user = this.state.user;
    user.balance += parseInt(chargeAmount);
    this.setState({
      user: user
    })
  }

  render() {
    return (
      <div>
        <Header
          isProfilePage={true}
          user={this.state.user}
        />
        <div className="content">
          <div className="container">
            <ProfileContent
              user={this.state.user}
              onSuccessfulCharge={this.onSuccessfulCharge}
            />
          </div>
        </div>
      </div>
    )
  }
}

class ProfileContent extends React.Component {

  onSuccessfulCharge = (chargeAmount) => {
    this.props.onSuccessfulCharge(chargeAmount);
  }

  render() {
    return (
      <HashRouter>
        <div className="row">
          <div className="container top-double-buffer">
            <div className="btn-group btn-group-toggle profile-button-group" data-toggle="buttons">
              <NavLink
                exact to="/charge"
                type="button"
                className="btn btn-toggle btn-secondary"
              >
                افزایش اعتبار
                </NavLink>

              <NavLink
                exact to="/orders"
                type="button"
                className="btn btn-toggle btn-secondary"
              >
                سفارش‌ها
                </NavLink>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="container">
            <div className="content-box top-bottom-double-buffer">
              <Route
                exact path="/charge"
                render={(props) => (<ChargeTab onSuccessfulCharge={this.onSuccessfulCharge} {...props}/>)}
              />
              <Route
                exact path="/orders"
                render={(props) => (<OrdersTab orders={this.props.user ? this.props.user.orders : []} {...props}/>)}
              />
            </div>
          </div>
        </div>
      </HashRouter>
    )
  }
}

export default Profile
