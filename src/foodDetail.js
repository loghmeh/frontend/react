import React from "react";
import Counter from './counter';
import starIcon from './assets/icons/star.svg'
class FoodDetail extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }
    componentDidMount() {
        this.setState({
            counter: parseInt(this.props.food.count),
        });
    }

    handleIncrease = () => {
        const food = this.props.food;
        if(this.props.countInCart === 0){
            this.props.handleAdd(food);
        }
        else {
            this.props.handleIncrease(food);
        }
    }
    handleDecrease = () => {
        const food = this.props.food;
        if (this.props.count === 0) {
            alert('you cannot decrease');
        }
        else {
            this.props.handleDecrease(food);
        }
    }
    addToCart() {
        const food = this.props.food;
        if(this.props.countInCart === 0){
            this.props.handleAdd(food);
        }
        else {
            this.props.handleIncrease(food);
        }
    }
    render() {
        const food = this.props.food;
        const counter = this.props.food.count;
        const restaurantName = this.props.restaurantName;
        return (
            <>
                <div className="row top-buffer justify-content-center">
                    <span className="fs-little font-bold">{restaurantName}</span>
                </div>
                <div className="row border-bottom">
                    <div className="col-3">
                    <img src={food.image} className="rounded img-fluid food-thumbnail"
                         alt="food-thumbnail" />
                    </div>
                    <div className="col">
                        <div className="row">
                            <span className="my-auto fs-tiny ml-2 font-bold">{food.name}</span>
                            <span className="d-inline-block my-auto mr-2">
                            <span className="fs-tiny font-thin">{food.popularity}</span>
                            <span>
                                <img className="icon-star" src={starIcon} alt="star" />
                            </span>
                        </span>
                        </div>
                        <div className="row">
                            <span className="fs-tiny font-thin description-text">{food.description} </span>
                        </div>
                        <div className="row">
                            <span className="fs-tiny font-thin">{food.price} تومان</span>
                        </div>
                    </div>
                </div>



                <div className="row bottom-buffer detail-finalize">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <Counter
                                    value={counter}
                                    onIncrease={this.handleIncrease}
                                    item onDecrease={this.handleDecrease}
                                />
                            </div>
                            <div className=" col-md-6">
                                <button type="button" className="btn btn-add-to-cart-detail" onClick={this.addToCart}>اضافه کردن به سبد خرید
                                </button>
                            </div>

                        </div>
                    </div>

                </div>
            </>
        )
    }
}
export default FoodDetail
