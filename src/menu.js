import React from "react";
import starLogo from './assets/icons/star.svg'
import Modal from './modal';
import FoodDetail from "./foodDetail";

class MenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.state = {
            show: false
        }
    }
    showModal = () => {
        this.setState({ show: true });
    };
    hideModal = () => {
        this.setState({ show: false });
    };
    handleAdd = ()=>{
        const foodId = this.props.food.id;
        this.props.onAdd(foodId);
    }
    handleIncrease= ()=> {
        this.props.handleIncrease(this.props.food.id);
    }
    handleDecrease=() =>{
        this.props.handleDecrease(this.props.food.id);
    }
    render() {
        const food = this.props.food;
        const restaurantId = this.props.restaurantId;
        const restaurantName = this.props.restaurantName;
        return (
            <div className="col-md-4 food-menu-item bottom-double-buffer">
                <Modal show={this.state.show} onClose={this.hideModal}>
                    <FoodDetail
                        food={food}
                        restaurantId={restaurantId}
                        restaurantName={restaurantName}
                        countInCart={this.props.countInCart}
                        handleIncrease={this.handleIncrease}
                        handleAdd={this.handleAdd}
                        handleDecrease={this.handleDecrease}
                    />
                </Modal>
                <div className="container h-100 bg-white-bordered">
                    <div className="row top-buffer justify-content-center">
                        <img src={food.image} className="rounded img-fluid food-thumbnail" alt="food-thumbnail" />
                    </div>
                    <div className="row justify-content-center">
                        <div className="container">
                            <span
                                className="my-auto fs-tiny ml-2 font-bold"
                                onClick={this.showModal}
                            >
                                {food.name}
                            </span>

                            <span className="d-inline-block my-auto mr-2">
                                <span className="fs-tiny font-thin">{food.popularity}</span>
                                <span>
                                    <img className="icon-star" src={starLogo} alt="star" />
                                </span>
                            </span>
                        </div>
                    </div>

                    <div className="row justify-content-center">
                        <div className="container">
                            <span className="fs-tiny font-thin">{food.price} تومان</span>
                        </div>
                    </div>

                    <div className="row bottom-buffer justify-content-center">
                        <div className="container">
                            <button type="button" onClick={this.handleAdd} className="btn btn-add-to-cart">افزودن به سبد خرید</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }
    addToCart = (food) =>{
        this.props.addToCart(food);
    }
    handleIncrease = (foodId) =>{
        this.props.increase(foodId);
    }
    handleDecrease = (foodId) => {
        this.props.decrease(foodId);
    }
    render() {
        const foods = this.props.foods;
        const restaurantId = this.props.restaurantId;
        const restaurantName = this.props.restaurantName;
        const listFoods = Object.values(foods).map((food) => {
            let count = 0;
            const foodIfInCart = this.props.cartItems.find((item) =>
                item.id == food.id
            )
            if (foodIfInCart) {
                count = foodIfInCart.count;
            }
            return (
                <MenuItem
                    key={food.id}
                    food={food}
                    countInCart={count}
                    restaurantId={restaurantId}
                    restaurantName={restaurantName}
                    onAdd={(food) => this.addToCart(food)}
                    handleIncrease={this.handleIncrease}
                    handleDecrease={this.handleDecrease}
                />
            );
        });
        return (
            <div className="col-md-8 foods-in-menu border-right mb-4">
                <div className="container">
                    <div className="row food-menu-row">
                        {listFoods}
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu
