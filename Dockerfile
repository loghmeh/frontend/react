FROM node:10-alpine as builder
COPY package.json package-lock.json ./
RUN npm install --quiet && mkdir /loghmeh-ui && mv ./node_modules ./loghmeh-ui
WORKDIR /loghmeh-ui
COPY . .
RUN npm run --silent build:prod

FROM nginx:alpine
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /loghmeh-ui/build /usr/share/nginx/html
ENTRYPOINT ["nginx", "-g", "daemon off;"]
